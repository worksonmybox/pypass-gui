from pypass_ui import Ui_PyPass
import sys
from PyQt5.QtGui import QKeySequence, QPalette, QColor
from PyQt5.QtCore import Qt
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QWidget, QMessageBox
import json
import string
import os
import pathlib
import secrets
import random
import time
import requests
import pyperclip
import csv
import hashlib
import asyncio
import aiohttp
import math
from pathlib import Path


app = QtWidgets.QApplication(sys.argv)
app.setStyle("Fusion")


class PyPassApp(Ui_PyPass):
    def __init__(self, window):
        self.setupUi(window)
        self.get_configs()
        self.set_dark_mode()
        # connect buttons
        self.dark_mode_button.clicked.connect(self.set_dark_mode)
        self.get_defaults_button.clicked.connect(self.on_get_defaults_button_clicked)
        self.set_defaults_button.clicked.connect(self.on_set_defaults_button_clicked)
        self.generate_password_button.clicked.connect(
            self.on_generate_password_button_clicked
        )
        self.use_special_checkbox.toggled.connect(self.on_use_special_toggled)
        self.use_nums_checkbox.toggled.connect(self.on_use_nums_toggled)
        self.clear_list_button.clicked.connect(self.on_clear_list_button_clicked)
        self.generated_passwords_list_widget.itemSelectionChanged.connect(
            self.on_list_item_selected
        )
        self.copy_selected_button.clicked.connect(self.on_copy_selected_button_clicked)
        self.copy_all_button.clicked.connect(self.on_copy_all_button_clicked)
        self.show_password_checkbox.stateChanged.connect(self.on_show_password_checked)
        self.test_password_line.textChanged.connect(self.on_test_password_changed)
        self.test_password_line.textChanged.connect(self.hash_password)
        self.test_password_line.textChanged.connect(self.calculate_entropy)
        self.copy_hash_button.clicked.connect(self.on_copy_hash_clicked)
        self.clear_hash_button.clicked.connect(self.on_clear_hash_clicked)
        self.clear_password_button.clicked.connect(self.on_clear_password_clicked)
        self.clear_entropy_button.clicked.connect(self.on_clear_entropy_clicked)
        self.clear_selection_button.clicked.connect(self.on_clear_selection_clicked)
        self.remove_selected_button.clicked.connect(self.on_remove_selected_clicked)
        self.test_password_button.clicked.connect(self.on_test_password_clicked)
        self.what_is_entropy_button.clicked.connect(self.on_what_is_entropy_clicked)

        # translations
        self._translate = QtCore.QCoreApplication.translate

    def on_test_password_changed(self):
        if "\n" in str(self.test_password_line.text()):
            self.test_password_line.setText(
                self.test_password_line.text().split("\n")[0]
            )

    def on_what_is_entropy_clicked(self):
        message = QMessageBox()
        message.setWindowTitle("What is Entropy?")
        message.setText(
            f"Password entropy is a measure of the strength of a password, and is a function of the permissible character set (or pool) and the length of the password - expressed in bits.\n\nGeneral guidelines:\n< 28 = extremely weak\n28 - 35 = very weak\n36 - 59 = weak\n60 - 127 = strong\n128+ = very strong\n"
        )
        message.exec_()

    def on_test_password_clicked(self):
        if self.test_password_line:
            self.test_password(self.test_password_line.text())

    def test_password(self, password):
        hashed_password = self.generate_hash(password)
        check = self.check_hash(len(password), password, hashed_password)
        if check:
            message = QMessageBox()
            message.setWindowTitle("Success!")
            message.setText(
                f"Your password was not found in any of haveibeenpwned's known breached passwords! \n\nNote: If this password is not randomly generated or is a password you have used before, you shouldn't use it even if it's not in any known breaches!"
            )
            message.exec_()

    def on_clear_selection_clicked(self):
        self.generated_passwords_list_widget.clearSelection()

    def on_remove_selected_clicked(self):
        for item in self.generated_passwords_list_widget.selectedItems():
            row = self.generated_passwords_list_widget.row(item)
            taken = self.generated_passwords_list_widget.takeItem(row)
            del taken

    def on_clear_entropy_clicked(self):
        self.entropy_number_label.setText("")

    def on_clear_password_clicked(self):
        self.test_password_line.setText("")

    def on_clear_hash_clicked(self):
        self.sha1_number_label.setText("")

    def on_copy_hash_clicked(self):
        pyperclip.copy(self.sha1_number_label.text())

    def on_show_password_checked(self):
        if self.test_password_line.echoMode() == 0:
            self.test_password_line.setEchoMode(2)
        else:
            self.test_password_line.setEchoMode(0)

    def on_clear_list_button_clicked(self):
        self.generated_passwords_list_widget.clear()

    def on_copy_selected_button_clicked(self):
        password_str = ""
        length = len(self.generated_passwords_list_widget.selectedItems())
        for item in self.generated_passwords_list_widget.selectedItems():
            if length > 1:
                password_str += f"{item.text()}\n"
            else:
                password_str += f"{item.text()}"

        pyperclip.copy(password_str)
        password_str = ""

    def on_copy_all_button_clicked(self):
        password_str = ""
        length = self.generated_passwords_list_widget.count()
        for item in range(length):
            if length > 1:
                password_str += (
                    f"{self.generated_passwords_list_widget.item(item).text()}\n"
                )
            else:
                password_str += (
                    f"{self.generated_passwords_list_widget.item(item).text()}"
                )

        pyperclip.copy(password_str)
        password_str = ""

    def on_list_item_selected(self):
        selected_item = self.generated_passwords_list_widget.currentItem()
        text = ""
        if selected_item:
            text = selected_item.text()

    def set_dark_mode(self):
        # print("DARK MODE")
        _translate = QtCore.QCoreApplication.translate
        configs = self.load_configs()

        if self.dark_mode:
            # print("ON")
            darkPalette = QPalette()
            # base
            darkPalette.setColor(QPalette.WindowText, QColor(180, 180, 180))
            darkPalette.setColor(QPalette.WindowText, Qt.white)
            darkPalette.setColor(QPalette.Button, QColor(53, 53, 53))
            darkPalette.setColor(QPalette.Light, QColor(180, 180, 180))
            darkPalette.setColor(QPalette.Midlight, QColor(90, 90, 90))
            darkPalette.setColor(QPalette.Dark, QColor(35, 35, 35))
            darkPalette.setColor(QPalette.Text, Qt.white)
            darkPalette.setColor(QPalette.BrightText, QColor(180, 180, 180))
            darkPalette.setColor(QPalette.ButtonText, Qt.white)
            darkPalette.setColor(QPalette.Base, QColor(42, 42, 42))
            darkPalette.setColor(QPalette.Window, QColor(53, 53, 53))
            darkPalette.setColor(QPalette.Shadow, QColor(20, 20, 20))
            darkPalette.setColor(QPalette.Highlight, QColor(42, 130, 218))
            darkPalette.setColor(QPalette.HighlightedText, Qt.white)
            darkPalette.setColor(QPalette.Link, QColor(56, 252, 196))
            darkPalette.setColor(QPalette.AlternateBase, QColor(66, 66, 66))
            darkPalette.setColor(QPalette.ToolTipBase, QColor(53, 53, 53))
            darkPalette.setColor(QPalette.ToolTipText, Qt.white)
            darkPalette.setColor(QPalette.LinkVisited, QColor(80, 80, 80))
            # disabled
            darkPalette.setColor(
                QPalette.Disabled, QPalette.WindowText, QColor(127, 127, 127)
            )
            darkPalette.setColor(
                QPalette.Disabled, QPalette.Text, QColor(127, 127, 127)
            )
            darkPalette.setColor(
                QPalette.Disabled, QPalette.ButtonText, QColor(127, 127, 127)
            )
            darkPalette.setColor(
                QPalette.Disabled, QPalette.Highlight, QColor(80, 80, 80)
            )
            darkPalette.setColor(
                QPalette.Disabled, QPalette.HighlightedText, QColor(127, 127, 127)
            )

            app.setPalette(darkPalette)
            self.dark_mode_button.setText(_translate("PyPass", "Light Mode"))

        else:
            # print("OFF")
            lightPalette = QPalette()
            # base
            lightPalette.setColor(QPalette.WindowText, QColor(0, 0, 0))
            lightPalette.setColor(QPalette.Button, QColor(240, 240, 240))
            lightPalette.setColor(QPalette.Light, QColor(180, 180, 180))
            lightPalette.setColor(QPalette.Midlight, QColor(200, 200, 200))
            lightPalette.setColor(QPalette.Dark, QColor(225, 225, 225))
            lightPalette.setColor(QPalette.Text, QColor(0, 0, 0))
            lightPalette.setColor(QPalette.BrightText, QColor(0, 0, 0))
            lightPalette.setColor(QPalette.ButtonText, QColor(0, 0, 0))
            lightPalette.setColor(QPalette.Base, QColor(255, 255, 255))
            lightPalette.setColor(QPalette.Window, QColor(240, 240, 240))
            lightPalette.setColor(QPalette.Shadow, QColor(20, 20, 20))
            lightPalette.setColor(QPalette.Highlight, QColor(76, 163, 224))
            lightPalette.setColor(QPalette.HighlightedText, QColor(0, 0, 0))
            lightPalette.setColor(QPalette.Link, QColor(0, 162, 232))
            lightPalette.setColor(QPalette.AlternateBase, QColor(225, 225, 225))
            lightPalette.setColor(QPalette.ToolTipBase, QColor(240, 240, 240))
            lightPalette.setColor(QPalette.ToolTipText, QColor(0, 0, 0))
            lightPalette.setColor(QPalette.LinkVisited, QColor(222, 222, 222))

            # disabled
            lightPalette.setColor(
                QPalette.Disabled, QPalette.WindowText, QColor(115, 115, 115)
            )
            lightPalette.setColor(
                QPalette.Disabled, QPalette.Text, QColor(115, 115, 115)
            )
            lightPalette.setColor(
                QPalette.Disabled, QPalette.ButtonText, QColor(115, 115, 115)
            )
            lightPalette.setColor(
                QPalette.Disabled, QPalette.Highlight, QColor(190, 190, 190)
            )
            lightPalette.setColor(
                QPalette.Disabled, QPalette.HighlightedText, QColor(115, 115, 115)
            )

            app.setPalette(lightPalette)

            self.dark_mode_button.setText(_translate("PyPass", "Dark Mode"))

        configs.update({"dark_mode": self.dark_mode})
        self.write_configs(configs)
        self.dark_mode = not self.dark_mode

    def write_configs(self, configs):
        path = Path(__file__).parent.resolve()
        with open(f"{path}/config.json", "w") as config:
            json.dump(configs, config)

    def load_configs(self):
        path = Path(__file__).parent.resolve()
        configs = dict()
        with open(f"{path}/config.json", "r") as config_file:
            configs = json.load(config_file)

        return configs

    def get_configs(self):
        configs = self.load_configs()
        self.length_spin.setValue(configs["length_spin"])
        self.number_of_passwords_spin.setValue(configs["number_of_passwords_spin"])
        self.exclude_line.setText(configs["exclude_line"])
        self.include_line.setText(configs["include_line"])
        self.num_special_spin.setValue(configs["num_special_spin"])
        self.num_digits_spin.setValue(configs["num_digits_spin"])
        self.no_repeat_checkbox.setChecked(configs["no_repeat_checkbox"])
        self.use_lower_checkbox.setChecked(configs["use_lower_checkbox"])
        self.use_upper_checkbox.setChecked(configs["use_upper_checkbox"])
        self.use_special_checkbox.setChecked(configs["use_special_checkbox"])
        self.use_nums_checkbox.setChecked(configs["use_nums_checkbox"])
        self.verify_checkbox.setChecked(configs["verify_checkbox"])
        self.dark_mode = configs["dark_mode"]

    def on_get_defaults_button_clicked(self):
        self.get_configs()

    def on_set_defaults_button_clicked(self):
        dark_mode = self.load_configs()["dark_mode"]
        configs = dict()
        configs["length_spin"] = self.length_spin.value()
        configs["number_of_passwords_spin"] = self.number_of_passwords_spin.value()
        configs["exclude_line"] = self.exclude_line.text()
        configs["include_line"] = self.include_line.text()
        configs["num_special_spin"] = self.num_special_spin.value()
        configs["num_digits_spin"] = self.num_digits_spin.value()
        configs["no_repeat_checkbox"] = self.no_repeat_checkbox.isChecked()
        configs["use_lower_checkbox"] = self.use_lower_checkbox.isChecked()
        configs["use_upper_checkbox"] = self.use_upper_checkbox.isChecked()
        configs["use_special_checkbox"] = self.use_special_checkbox.isChecked()
        configs["use_nums_checkbox"] = self.use_nums_checkbox.isChecked()
        configs["verify_checkbox"] = self.verify_checkbox.isChecked()
        configs["dark_mode"] = dark_mode
        self.write_configs(configs)

    def on_use_special_toggled(self):
        if self.use_special_checkbox.isChecked():
            self.num_special_spin.setDisabled(False)
            self.num_special_spin.setValue(1)
        else:
            self.num_special_spin.setDisabled(True)
            self.num_special_spin.setValue(0)

    def on_use_nums_toggled(self):
        if self.use_nums_checkbox.isChecked():
            self.num_digits_spin.setDisabled(False)
            self.num_digits_spin.setValue(1)
        else:
            self.num_digits_spin.setDisabled(True)
            self.num_digits_spin.setValue(0)

    def on_generate_password_button_clicked(self):
        if (
            not self.use_lower_checkbox.isChecked()
            and not self.use_upper_checkbox.isChecked()
            and not self.use_nums_checkbox.isChecked()
            and not self.use_special_checkbox.isChecked()
        ):
            message = QMessageBox()
            message.setWindowTitle("Error")
            message.setText(
                f"Cannot generate, no characters available based on user selections.\n\nLower: False\n\nUpper: False\n\nSpecial: False\n\nNumber: False"
            )
            message.exec_()
            return

        if self.num_special_spin.value() > self.length_spin.value():
            message = QMessageBox()
            message.setWindowTitle("Error")
            message.setText(
                f"Cannot generate, minimum number of special characters ({num_special_spin.value()}) cannot exceed the length of the password ({length_spin.value()})"
            )
            message.exec_()
            return

        if self.num_digits_spin.value() > self.length_spin.value():
            message = QMessageBox()
            message.setWindowTitle("Error")
            message.setText(
                f"Cannot generate, minimum number of numeric characters ({num_digits_spin.value()}) cannot exceed the length of the password ({length_spin.value()})"
            )
            message.exec_()
            return

        if (
            self.num_digits_spin.value() > len(string.digits)
            and self.no_repeat_checkbox.isChecked()
        ):
            message = QMessageBox()
            message.setWindowTitle("Error")
            message.setText(
                f"Cannot generate, minimum number of numeric characters ({num_digits_spin.value()}) cannot exceed the allowed length of non-repeating numbers ({len(string.digits)}).\n\nOption 1 (Recommended): Reduce the minimum number of required numeric characters to be <= {len(string.digits)}\n\nOption 2: Allow repeating characters"
            )
            message.exec_()
            return

        if (
            self.num_special_spin.value() > len(string.punctuation)
            and self.no_repeat_checkbox.isChecked()
        ):
            message = QMessageBox()
            message.setWindowTitle("Error")
            message.setText(
                f"Cannot generate, minimum number of special characters ({num_special_spin.value()}) cannot exceed the allowed length of non-repeating special characters ({len(string.punctuation)}).\n\nOption 1 (Recommended): Reduce the minimum number of required special characters to be <= {len(string.punctuation)}\n\nOption 2: Allow repeating characters"
            )
            message.exec_()
            return

        if self.exclude_line.text() and self.include_line.text():
            for c in self.exclude_line.text():
                if c in self.include_line.text():
                    message = QMessageBox()
                    message.setWindowTitle("Error")
                    message.setText(
                        f"Cannot explicitly exclude and include the same character\n\nCharacter: {c}"
                    )
                    message.exec_()
                    return

        # alert = QMessageBox()

        allowed_chars = self.get_allowed_chars(
            self.use_lower_checkbox.isChecked(),
            self.use_upper_checkbox.isChecked(),
            self.use_nums_checkbox.isChecked(),
            self.use_special_checkbox.isChecked(),
        )

        password_length = self.length_spin.value()

        if (
            len(allowed_chars) < password_length
        ) and self.no_repeat_checkbox.isChecked():
            message = QMessageBox()
            message.setWindowTitle("Error")
            message.setText(
                f"Cannot generate, password length ({password_length}) exceeds length of allowed non-repeating characters ({len(allowed_chars)}).\n\nOption 1 (Recommended): Allow other character types (eg. Lowers, Uppers, Specials, Numbers); the more the better\n\nOption 2: Reduce the password length requirement to be <= the allowed characters\n\nOption 3: Allow repeat characters."
            )
            message.exec_()
            return

        passwords = self.pypass(
            password_length,
            self.number_of_passwords_spin.value(),
            False,
            self.exclude_line.text(),
            self.include_line.text(),
            self.no_repeat_checkbox.isChecked(),
            self.verify_checkbox.isChecked(),
            self.num_special_spin.value(),
            self.num_digits_spin.value(),
            allowed_chars,
        )

        for pw in passwords:
            self.generated_passwords_list_widget.addItem(pw)

    def get_allowed_chars(self, use_lower, use_upper, use_nums, use_special):
        allowed_chars = ""

        if use_lower:
            allowed_chars += string.ascii_lowercase
        if use_upper:
            allowed_chars += string.ascii_uppercase
        if use_nums:
            allowed_chars += string.digits
        if use_special:
            allowed_chars += string.punctuation

        if self.include_line.text():
            for c in self.include_line.text():
                if c not in allowed_chars:
                    allowed_chars += c

        return allowed_chars

    def pypass(
        self,
        pw_length,
        num_pw,
        write,
        exclude,
        include,
        no_repeat,
        verify,
        num_special,
        num_digits,
        allowed_chars,
    ):
        """Generates a random password using Python 3.6's secrets library, runs it through the SHA1 hashing algorithm, and checks it against
        haveibeenpwned.com's password range API. If the password is good, it conveniently copies it to the clipboard (for single passwords),
        else it will print/write to CSV, the list of passwords. If the password fails the test, a new one will be generated and returned."""

        passwords = []
        hashed_passwords = []
        hashes = []

        if num_pw == 1:
            password = self.create_password(
                pw_length,
                exclude,
                include,
                no_repeat,
                num_special,
                num_digits,
                allowed_chars,
            )
            if verify:
                hashed_password = self.generate_hash(password)
                self.check_hash(pw_length, password, hashed_password)
            return [password]
        elif num_pw > 1:
            # synchronous
            # while num_pw > 0:
            #     password = self.create_password(
            #         pw_length,
            #         exclude,
            #         include,
            #         no_repeat,
            #         num_special,
            #         num_digits,
            #         allowed_chars,
            #     )
            #     if verify:
            #         hashed_password = self.generate_hash(password)
            #         good_password = self.check_hash(pw_length, password, hashed_password)
            #         passwords.append(good_password)
            #     else:
            #         passwords.append(password)

            #     num_pw -= 1

            # asynchronous
            while num_pw > 0:
                password = self.create_password(
                    pw_length,
                    exclude,
                    include,
                    no_repeat,
                    num_special,
                    num_digits,
                    allowed_chars,
                )
                if verify:
                    hashed_password = self.generate_hash(password)
                    passwords.append(password)
                    hashed_passwords.append(hashed_password)
                    hashes.append(
                        {"password": password, "hashed_password": hashed_password}
                    )
                else:
                    passwords.append(password)

                num_pw -= 1

            if verify:
                asyncio.run(self.async_check_all_hashes(pw_length, hashes))

            if write:
                with open("passwords.csv", "w", newline="") as csv_file:
                    csv_writer = csv.writer(csv_file)
                    for pw in passwords:
                        csv_writer.writerow([pw])
            else:
                return passwords

        elif num_pw <= 0:
            return (
                "Error: Number of passwords to be generated must be greater than zero."
            )

    def create_password(
        self,
        pw_length,
        exclude,
        include,
        no_repeat,
        num_special,
        num_digits,
        allowed_chars,
    ):

        password = "".join(secrets.choice(allowed_chars) for i in range(pw_length))

        special_count = 0
        num_count = 0

        if exclude and no_repeat:
            char_list = []
            for c in password:
                if c in string.punctuation:
                    special_count += 1
                elif c in string.digits:
                    num_count += 1
                while c in exclude or c in char_list:
                    c = self.create_char(allowed_chars)
                char_list.append(c)
            password = "".join(char_list)

        elif exclude:
            for char in exclude:
                char_list = []
                for c in password:
                    if c in string.punctuation:
                        special_count += 1
                    elif c in string.digits:
                        num_count += 1
                    while c in exclude:
                        c = self.create_char(allowed_chars)
                    char_list.append(c)
                password = "".join(char_list)

        elif no_repeat:
            char_list = []
            for c in password:
                if c in string.punctuation:
                    special_count += 1
                elif c in string.digits:
                    num_count += 1
                while c in char_list:
                    c = self.create_char(allowed_chars)
                char_list.append(c)
            password = "".join(char_list)

        else:
            for c in password:
                if c in string.punctuation:
                    special_count += 1
                elif c in string.digits:
                    num_count += 1

        if special_count < num_special or num_count < num_digits:
            special_count = 0
            num_count = 0
            try:
                return self.create_password(
                    pw_length,
                    exclude,
                    include,
                    no_repeat,
                    num_special,
                    num_digits,
                    allowed_chars,
                )
            except RecursionError:
                message = QMessageBox()
                message.setWindowTitle("Error")
                message.setText(
                    f"Oops! Looks like you caught me in a recursion error! This could be due to either the number of minimum special characters ({num_special}) or the number of minumum numeric characters ({num_digits}) or both of them summed ({num_special + num_digits}) being greater than half ({int(pw_length / 2)}) of the password length ({pw_length}).\n\nOption 1 (Recommended): Increase the length of your password!\n\nOption 2: Decrease the amount of required special characters or required numeric characters."
                )
                message.exec_()
                return

        return password

    def create_char(self, allowed_chars):
        return "".join(secrets.choice(allowed_chars) for i in range(1))

    def generate_hash(self, str_password):
        str_password = str_password.encode("utf-8")
        sha1_obj = hashlib.sha1(str_password)
        hashed_password = sha1_obj.hexdigest()

        return hashed_password

    def hash_password(self):
        password = self.test_password_line.text().encode("utf-8")
        sha1_obj = hashlib.sha1(password)
        hashed_password = sha1_obj.hexdigest()
        self.sha1_number_label.setText(hashed_password)
        if self.test_password_line.text().strip() == "":
            self.sha1_number_label.setText("")

    def check_hash(self, pw_length, password, hashed_password):
        first_five = hashed_password[0:5].upper()
        remaining = hashed_password[5:].upper()
        try:
            r = requests.get(
                "https://api.pwnedpasswords.com/range/{}".format(first_five)
            )
        except TimeoutError as te:
            print(te)
            print(
                "Try checking your internet connection, if you know it's good, try checking \
                your firewall/proxy settings."
            )
            message = QMessageBox()
            message.setWindowTitle("TimeoutError")
            message.setText(
                f"Oops! Looks like we timed out trying to verify your password wasn't in a breach using https://haveibeenpwned.com, Try checking your internet connection, if you know it's good, try checking your firewall/proxy settings."
            )
            message.exec_()
            return
        except ConnectionError as ce:
            print(ce)
            print(
                "Try checking your internet connection, if you know it's good, try checking \
                your firewall/proxy settings."
            )
            message = QMessageBox()
            message.setWindowTitle("ConnectionError")
            message.setText(
                f"Oops! Looks like we had a connection error trying to verify your password wasn't in a breach using https://haveibeenpwned.com, Try checking your internet connection, if you know it's good, try checking your firewall/proxy settings."
            )
            message.exec_()
            return
        r_list = r.text.split("\r\n")
        for item in r_list:
            current_hash = item.split(":")[0]
            if remaining == current_hash:
                if pw_length:
                    message = QMessageBox()
                    message.setWindowTitle("Breached!")
                    message.setText(
                        f"Oops! It looks like this password was found in haveibeenpwned's list of breached passwords, don't use this one!"
                    )
                    message.exec_()
                    return
                else:
                    return

        return password

    async def async_check_all_hashes(self, pw_length, hashes):
        async with aiohttp.ClientSession() as session:
            tasks = []
            for h in hashes:
                task = asyncio.create_task(
                    self.async_check_hash(
                        pw_length, h["password"], h["hashed_password"], session
                    )
                )
                tasks.append(task)
            await asyncio.gather(*tasks, return_exceptions=True)

    async def async_check_hash(self, pw_length, password, hashed_password, session):
        first_five = hashed_password[0:5].upper()
        remaining = hashed_password[5:].upper()
        try:
            async with session.get(
                f"https://api.pwnedpasswords.com/range/{first_five}"
            ) as response:
                response_text = await response.text()
        except TimeoutError as te:
            print(te)
            print(
                "Try checking your internet connection, if you know it's good, try checking \
                your firewall/proxy settings."
            )
            message = QMessageBox()
            message.setWindowTitle("TimeoutError")
            message.setText(
                f"Oops! Looks like we timed out trying to verify your password wasn't in a breach using https://haveibeenpwned.com, Try checking your internet connection, if you know it's good, try checking your firewall/proxy settings."
            )
            message.exec_()
            return
        except ConnectionError as ce:
            print(ce)
            print(
                "Try checking your internet connection, if you know it's good, try checking \
                your firewall/proxy settings."
            )
            message = QMessageBox()
            message.setWindowTitle("ConnectionError")
            message.setText(
                f"Oops! Looks like we had a connection error trying to verify your password wasn't in a breach using https://haveibeenpwned.com, Try checking your internet connection, if you know it's good, try checking your firewall/proxy settings."
            )
            message.exec_()
            return
        responses = response_text.split("\r\n")
        for item in responses:
            current_hash = item.split(":")[0]
            if remaining == current_hash:
                if pw_length:
                    message = QMessageBox()
                    message.setWindowTitle("Breached!")
                    message.setText(
                        f"Oops! It looks like this password was found in haveibeenpwned's list of breached passwords, don't use this one!"
                    )
                    message.exec_()
                    return
                else:
                    return

        return password

    def calculate_entropy(self):
        password = self.test_password_line.text()
        password_length = len(password)
        pool = self.calculate_pool()
        possible = float()
        try:
            possible = math.pow(int(pool), int(password_length))
        except OverflowError:
            message = QMessageBox()
            message.setWindowTitle("Breached!")
            message.setText(
                f"Oops! It looks like your password is so long that we can't reasonably calculate entropy!"
            )
            message.exec_()
            return

        entropy = math.log2(possible)
        if entropy:
            self.entropy_number_label.setText(str(int(entropy)))
        else:
            self.entropy_number_label.setText("")

        return entropy

    def calculate_pool(self):
        password = self.test_password_line.text()

        lower = False
        upper = False
        number = False
        special = False

        for char in password:
            if char in string.ascii_lowercase:
                lower = True
            elif char in string.ascii_uppercase:
                upper = True
            elif char in string.digits:
                number = True
            elif char in string.punctuation:
                special = True

        pool = 0
        if lower:
            pool += len(string.ascii_lowercase)
            if upper:
                pool += len(string.ascii_uppercase)
                if number:
                    pool += len(string.digits)
                    if special:
                        pool += len(string.punctuation)
            elif number:
                pool += len(string.digits)
                if special:
                    pool += len(string.punctuation)
            elif special:
                pool += len(string.punctuation)
        elif upper:
            pool += len(string.ascii_uppercase)
            if number:
                pool += len(string.digits)
                if special:
                    pool += len(string.punctuation)
            elif number:
                pool += len(string.digits)
                if special:
                    pool += len(string.punctuation)
            elif special:
                pool += len(string.punctuation)
        elif number:
            pool += len(string.digits)
            if special:
                pool += len(string.punctuation)
        elif special:
            pool += len(string.punctuation)

        return pool


PyPass = QtWidgets.QWidget()
ui = PyPassApp(PyPass)
PyPass.show()
sys.exit(app.exec_())
