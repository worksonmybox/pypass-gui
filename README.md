# PyPass-GUI

A password generator built in Python.

![](PyPass-GUI_Dark.png)
![](PyPass-GUI_Light.png)


## Requirements

PyPass requires the following:
- Python >= 3.7
- PyQT5 (pip)
- pyperclip (pip)
- asyncio (pip)
- aiohttp (pip)
- xclip or another clipboard manager to be installed (apt or similar)


## Use

Once you have the requirements downloaded and installed, cd into the directory after it is unzipped and run <code>python3 pypass.py</code> in your terminal - this should bring up the GUI.

From here, you choose your settings and click Generate Password, and it will be placed in the generated passwords list for use. Make sure you save these into your password manager (if you don't have one, you should definitely get one!).


## Tips

The include text box is very useful for those websites that allow *certain* special characters, but only allow a limited set. If you uncheck the "Special" checkbox, and add the allowed special characters into the "Include" text box, only those which are allowed will be in the pool of possible characters.


## Shortcuts

Generate Password = CTRL + G

Clear Generated Passwords List = CTRL + L

Test password = CTRL + T

Copy All Generated Passwords = CTRL + C

Copy Hash = CTRL + H



## Information for nerds

This was written using a cryptographically secure random character generator available in the secrets library of Python 3.6 and above. It also utilizes asyncio operations in order to speed up verification when users opt to generate/verify multiple (up to 5 at a time) passwords.

Entropy is calculated for any password that is typed/copied into the password text box in the "Test" section. 

For those that don't know what entropy is, it is a measure of the strength of a password, and is a function of the permissible character set (or pool) and the length of the password - expressed in bits. See the "What's this?" button at the bottom right of PyPass for estimations on what entropy *should* be in my opinion. If this generator is used with the defaults, you'll be well off.

Any characters added into the "Include" text box are not guaranteed to make it into your generated password(s) - they are simply added into the pool of possible characters to be chosen by the random generator - this preserves the cryptographically secure nature of said generator. Conversely, any characters added to the "Exclude" text box guarantee that those characters will not be in any generated password(s) as they are removed from the pool of possible characters. 


## Notes

This program requires an internet connection if the "verify" checkbox is checked when generating passwords or if you use the "test password" functionality. These two reach out to the haveibeenpwned password [range API](https://haveibeenpwned.com/API/v3#SearchingPwnedPasswordsByRange). Basically what this does is accept the first five characters of a sha1 hashed password and returns a list of passwords that share the first five characters of the sha1 hash, we then take that list and verify that our full sha1 hash of our password is not in that list which tells us that the password we are testing hasn't been in any known breaches. This is a security measure so we aren't sending our passwords or full sha1 hashes of them out over the internet. 

This is currently a single threaded program. If you don't know what that means, just know that if you're smashing the generate password button or the test password button relentlessly, the UI will eventually stop reacting until it catches up with your requests - especially when "verify" is checked and you are generating, and this is made worse on slower internet connections or slow-ish vpn connections. For the typical user and the typical use case, this shouldn't be an issue. I will be making this multithreaded in the near future in order to mitigate these issues for users who like to smash buttons or otherwise stress test things :).

## Roadmap

- Make PyPass multi-threaded so the UI doesn't lock up when users smash the generate/test buttons (made worse by slower internet connections).

- Package as a snap or AppImage or similar. 

